package com.yesia.onoffproject.model

import com.google.gson.annotations.SerializedName

data class ResponseEmployees(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)