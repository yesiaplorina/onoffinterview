package com.yesia.onoffproject.network

import com.yesia.onoffproject.model.ResponseEmployees
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RestAPI {

    @GET("v1/employees")
    fun getDataKaryawan(): Call<ResponseEmployees>
}