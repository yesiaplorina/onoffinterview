package com.yesia.onoffproject.network

import com.google.gson.GsonBuilder
import com.yesia.onoffproject.Helper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object InitRetrofit {
    val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    val client = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .retryOnConnectionFailure(true)
        .connectTimeout(15, TimeUnit.SECONDS)
        .build()
    val gson = GsonBuilder().setLenient().create()
    //URL Login dan register
    val retrofit = Retrofit.Builder()
        .baseUrl(Helper.BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    fun getInstance(): RestAPI = retrofit.create(RestAPI::class.java)
}