package com.yesia.onoffproject.presenter

import com.yesia.onoffproject.base.BaseView
import com.yesia.onoffproject.model.DataItem

interface EmployeesContract {
    interface Presenter {
        fun getDataKaryawan(
        )
    }

    interface View : BaseView {
        fun showMsg(msg: String?)
        fun showError(localizedMessage: String?)
        fun showLoading()
        fun hideLoading()
        fun pindahHalaman(dataKaryawan: List<DataItem?>?)
    }
}