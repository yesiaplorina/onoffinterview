package com.yesia.onoffproject.presenter

import com.yesia.onoffproject.base.BasePresenter
import com.yesia.onoffproject.model.ResponseEmployees
import com.yesia.onoffproject.network.InitRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EmployeesPresenter(var dataEmployees: EmployeesContract.View? = null) :
    BasePresenter<EmployeesContract.View>, EmployeesContract.Presenter {
    override fun onAttach(view: EmployeesContract.View) {
        dataEmployees = view
    }

    override fun onDettach() {
        dataEmployees = null
    }

    override fun getDataKaryawan() {

        dataEmployees?.showLoading()
        InitRetrofit.getInstance().getDataKaryawan().enqueue(object : Callback<ResponseEmployees> {
            override fun onFailure(call: Call<ResponseEmployees>, t: Throwable) {
                dataEmployees?.showError(t.localizedMessage)
                dataEmployees?.hideLoading()
            }

            override fun onResponse(
                call: Call<ResponseEmployees>,
                response: Response<ResponseEmployees>
            ) {
                dataEmployees?.hideLoading()
                if (response.isSuccessful) {
                    var status = response.body()?.status
                    var dataLengkap = response.body()?.data
                    if (status == "success"){
                        dataEmployees?.pindahHalaman(dataLengkap)
                    }
                }
            }
        })
    }
}