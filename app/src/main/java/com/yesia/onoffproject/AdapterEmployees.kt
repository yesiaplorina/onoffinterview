package com.yesia.onoffproject

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yesia.onoffproject.model.DataItem
import kotlinx.android.synthetic.main.menu_item_employees.view.*

class AdapterEmployees(
    var activities: Activity,
    var dataKaryawan: List<DataItem?>,
    sortedHomeFeed: List<DataItem?>?
) : RecyclerView.Adapter<AdapterEmployees.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var v = LayoutInflater.from(activities).inflate(R.layout.menu_item_employees, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int = dataKaryawan!!.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindItem(dataKaryawan?.get(position)!!, activities)
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(item: DataItem, activity: Activity) {

            itemView.tv_nama_karyawan.text = item.employeeName
            itemView.tv_gajih.text = item.employeeSalary
            itemView.tv_umur.text = item.employeeAge

        }

    }
}