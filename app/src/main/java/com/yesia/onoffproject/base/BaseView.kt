package com.yesia.onoffproject.base

interface BaseView {
    fun onAttachView()
    fun onDettachView()
}