package com.yesia.onoffproject.base

interface BasePresenter <T : BaseView> {
    fun onAttach(view: T)
    fun onDettach()
}