package com.yesia.onoffproject

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.yesia.onoffproject.model.DataItem
import com.yesia.onoffproject.model.ResponseEmployees
import com.yesia.onoffproject.presenter.EmployeesContract
import com.yesia.onoffproject.presenter.EmployeesPresenter
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import org.jetbrains.anko.toast
import java.io.IOException

class MainActivity : AppCompatActivity(), EmployeesContract.View {

    lateinit var presenter: EmployeesPresenter
    lateinit var spinner: Spinner
    var jsonUrl: String = "http://dummy.restapiexample.com/api/v1/employees"
    lateinit var dataKaryawan: List<DataItem?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinner = findViewById(R.id.spinner) as Spinner
        val options = arrayOf("All", "two", "three", "four")

        spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2 == 0) {
                    progressbar_main.visibility = View.VISIBLE
                    initPresenter()
                    presenter.getDataKaryawan()
                } else if (p2 == 1) {
                    fetchJson(jsonUrl)
                } else if (p2 == 2) {
                    toast("wewewew")
                } else if (p2 == 3) {
                    toast("zzzzzzzzzzz")
                }

            }
        }
//git
        progressbar_main.visibility = View.VISIBLE
        initPresenter()
        presenter.getDataKaryawan()
    }


    private fun fetchJson(jsonUrl: String) {
        val request = okhttp3.Request.Builder().url(jsonUrl).build()

        val client = OkHttpClient()
        client.newCall(request).equals(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()

                val gson = GsonBuilder().create()

                val allData = gson.fromJson(body, ResponseEmployees::class.java)
                val sortedHomeFeed = allData.data?.sortedWith(compareBy({ it?.employeeAge }))



                recyclerview_main.adapter = AdapterEmployees(
                    Activity(),
                    dataKaryawan, sortedHomeFeed
                )
                progressbar_main.visibility = View.VISIBLE
                initPresenter()
                presenter.getDataKaryawan()


            }
        })
    }


    fun <T> compareBy(vararg selectors: (T) -> Comparable<*>?): Comparator<T> {
        return Comparator<T> { a, b -> compareValuesBy(a, b, *selectors) }
    }

    private fun initPresenter() {
        presenter = EmployeesPresenter(this)
    }

    override fun showMsg(msg: String?) {
    }

    override fun showError(localizedMessage: String?) {
    }

    override fun showLoading() {
        progressbar_main.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressbar_main.visibility = View.INVISIBLE
    }

    override fun pindahHalaman(dataKaryawan: List<DataItem?>?) {
        var adapter = AdapterEmployees(this, dataKaryawan as List<DataItem?>, dataKaryawan)
        recyclerview_main.adapter = adapter
        recyclerview_main.layoutManager = LinearLayoutManager(this)
    }

    override fun onAttachView() {
        presenter.onAttach(this)
    }

    override fun onDettachView() {
        presenter.onDettach()
    }

    override fun onStart() {
        super.onStart()
        onAttachView()
    }

    override fun onDestroy() {
        super.onDestroy()
        onDettachView()
    }
}
